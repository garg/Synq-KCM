/*
  Copyright © 2011 Rohan Garg <rohan16garg@gmail.com>

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License as
  published by the Free Software Foundation; either version 2 of
  the License or (at your option) version 3 or any later version
  accepted by the membership of KDE e.V. (or its successor approved
  by the membership of KDE e.V.), which shall act as a proxy
  defined in Section 14 of version 3 of the license.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Module.h"
#include "ui_Module.h"

#include <QtCore/QFile>
#include <QtGui/QPaintEngine>

#include <KAboutData>
#include <KPluginFactory>
#include <KStandardDirs>

#include "Version.h"

K_PLUGIN_FACTORY_DECLARATION(KcmSynqConfigFactory);

Module::Module(QWidget *parent, const QVariantList &args) :
    KCModule(KcmSynqConfigFactory::componentData(), parent, args),
    ui(new Ui::Dialog)
{
    KAboutData *about = new KAboutData("kcm-synq", 0,
                                       ki18n("Synq Configuration"),
                                       global_s_versionStringFull,
                                       ki18n("Configure your synq accounts"),
                                       KAboutData::License_GPL_V3,
                                       ki18n("Copyright 2011 Rohan Garg"),
                                       KLocalizedString(), QByteArray(),
                                       "rohan16garg@gmail.com");

    about->addAuthor(ki18n("Rohan Garg"), ki18n("shadeslayer"), "rohan16garg@gmail.com");
    setAboutData(about);

    ui->setupUi(this);
        
    ui->addAccountButton->setIcon(KIcon("list-add"));
//    ui->editAccountButton->setIcon(KIcon("configure"));
    ui->removeAccountButton->setIcon(KIcon("edit-delete"));

    // We have no help so remove the button from the buttons.
    setButtons(buttons() ^ KCModule::Help);
}

Module::~Module()
{
    delete ui;
}
